[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)

# Tercera Práctica
## Resolución con paso de mensajes

Para la resolución de análisis y diseño se deberán utilizar paso de mensajes asíncronos como herramienta para la programación concurrente. También se utilizará la factoría [`Executors`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/Executors.html) y la interface [`ExecutorService`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/ExecutorService.html) para la ejecución de las tareas concurrentes que compondrán la solución de la práctica.

Para la implementación de la práctica se utilizará como herramienta de concurrencia JMS (Java Message Service). Esta práctica es una práctica en grupo de hasta dos alumnos y cada grupo deberá crear en el _broker_ sus propios _destinos_ para sus mensajes. Los miembros del grupo no tienen que pertenecer al mismo grupo de prácticas. Cada destino deberá definirse siguiendo la siguiente estructura:

```
....

// En la interface Constantes del proyecto

public static final String DESTINO = "ssccdd.curso2024.NOMBRE_GRUPO.BUZON";

...

```

El nombre del grupo tiene que ser único para los grupos, por lo que se recomienda usar alguna combinación de los nombres de los integrantes del grupo.

## Problema a resolver :  Servicio de Vacaciones

Hay que resolver un problema para un servicio de vacaciones que tendrá a su disposición rutas de vuelo o transporte terrestre a diferentes zonas. Además en esas zonas habrá hoteles donde los viajeros puedan alojarse en sus vacaciones. Las operaciones que se pueden realizar son las siguientes:

 - Consulta de la disponibilidad
 - Reserva para viajes y/o estancia
 - Pago, con o sin cancelación
 - Cancelación de reservas, si es posible hacerlo

Además hay dos tipos de procesos que podrán realizar las operaciones:

 1. Usuarios particulares
 2. Agencias de viajes que tendrán prioridad sobre usuarios particulares. Esta prioridad no puede ser estricta, es decir, los usuarios deben tener posibilidad para realizar sus operaciones.

Para su implementación en Java la herramienta será JMS en su modo asíncrono.

**NOTA:** 
Como la práctica es en grupos de dos personas cada uno de los componentes del equipo deberá encargarse de uno de los dos tipos de procesos que realizan las operaciones. El desarrollo del servicio deberá hacerse de forma conjunta. En el repositorio deberá quedar constancia de las aportaciones de cada uno de los miembros del equipo. El desarrollo de la documentación deberá estar correctamente coordinada y no debe ser una suma de dos trabajos individuales.
